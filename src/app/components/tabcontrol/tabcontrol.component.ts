import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { TextlistService } from 'src/app/services/textlist.service';


@Component({
  selector: 'app-tabcontrol',
  templateUrl: './tabcontrol.component.html',
  styleUrls: ['./tabcontrol.component.css']
})
export class TabcontrolComponent implements OnInit {
  listaOficial:string[]=[];
  lista:string[]=[];
  form!:FormGroup;
  v:boolean=true;
  constructor(
              private _snackbar:MatSnackBar,
              private fb:FormBuilder,
              ) {
    this.cargarForm1();
    this.cargarListaOficial();

   }

   get getListaform1(){
    return this.form.get('listatexto') as FormArray;
  }

  ngOnInit(): void {
  }


  cargarForm1(){
    this.form = this.fb.group({
      listatexto:this.fb.array([])
    });

    this.getListaform1.push(this.fb.group({
      texto: [null,[Validators.pattern(/^[a-zA-zñÑ\s]+$/)]],
      buttonValue : [null,Validators.required]
    }),
    );
    this.getListaform1.push(this.fb.group({
      texto: [null,[Validators.pattern(/^[a-zA-zñÑ\s]+$/)]],
      buttonValue : [null,Validators.required]
    }),
    );
    this.getListaform1.push(this.fb.group({
      texto: [null,[Validators.pattern(/^[a-zA-zñÑ\s]+$/)]],
      buttonValue : [null,Validators.required]
    }),
    );
    this.getListaform1.push(this.fb.group({
      texto: [null,[Validators.pattern(/^[a-zA-zñÑ\s]+$/)]],
      buttonValue : [null,Validators.required]
    }),
    );
    this.getListaform1.push(this.fb.group({
      texto: [null,[Validators.pattern(/^[a-zA-zñÑ\s]+$/)]],
      buttonValue : [null,Validators.required]
    }),
    );
    this.getListaform1.push(this.fb.group({
      texto: [null,[Validators.pattern(/^[a-zA-zñÑ\s]+$/)]],
      buttonValue : [null,Validators.required]
    }),
    );

     
    //console.log(this.getListaform1);
    
  }
  
  ponerValor(event:any,id:number){
    if (this.getListaform1.at(id).get('texto')?.valid) {
      if(event.keyCode==13){
       
        this.getListaform1.at(id).get('buttonValue')?.setValue(this.getListaform1.at(id).get('texto')?.value);
      }
    }
  }

  guardarAForm2(){
    this.cargarLista() 
    //console.log(this.lista);
     
  }

  //Formulario segundo___________________
  
  cargarLista(){
    
    for (let i = 0; i < this.getListaform1.length; i++) {
        if (this.getListaform1.at(i).get('buttonValue')?.valid) {
         // console.log(this.getListaform1.at(i).get('buttonValue')?.value == null);
          
          this.lista.push(this.getListaform1.at(i).get('buttonValue')?.value);
        }
    }

  }


  eliminar(id:number){
    const opcion = confirm('Estas seguro de eliminar el texto');
       if (opcion) {
        // this.lista =this.lista.filter(data => {
        //   return data!==id; 
        // })
    
        this.lista =this.lista.filter((data,i) => {
          return i!=id;
        })
       this._snackbar.open('El usuario fue eliminado con exito', '', {
         duration: 1500,
         horizontalPosition: 'center',
         verticalPosition: 'bottom',
       });
     }
  }

  addAll(){
    // this._listaService.agregarALista();
    // this._listaService.limpiarListaPrevia();
    // this.cargarLista();
    this.cargarListaOficial()
  }

  clear(){
    this.lista=[];
    // this.cargarLista();
  }

  //Formulario tres:--------------------------------------
  
  cargarListaOficial(){
    this.lista.forEach(e =>{
      this.listaOficial.push(e)
    });
  }


}
