import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class TextlistService {
listaPrevia:string[]=['text1','text2','text3'];
listaTexto:string[]=[];
  constructor() { }

  getTextos():string[]{
    return this.listaTexto.slice();
  }

  removeTexto(texto:string){
    this.listaTexto =this.listaTexto.filter(data => {
      return data!==texto; 
    })
  }

  agregarTexto(texto:string){
    this.listaTexto.push(texto);
  }

  agregarALista(){
    this.listaTexto = this.listaPrevia;
    return this.listaTexto;
  }

//----------------
  getTextosPrevios():string[]{
    return this.listaPrevia.slice();
  }

  removeTextoPrevio(texto:string){
    this.listaPrevia =this.listaPrevia.filter(data => {
      return data!==texto; 
    })
  }

  agregarTextoPrevio(texto:string){
    this.listaPrevia.push(texto);
  }

  limpiarListaPrevia(){
    this.listaPrevia=[];
  }
}
